//program to find prefix from infix
#include <stdio.h>
#include <stdlib.h>
#include<string.h>
struct Node
{
char data;
struct Node *next;
}*top=NULL;


void push(char x)
{
    struct Node *t;
    t=(struct Node*)malloc(sizeof(struct Node));

        t->data=x;
        t->next=top;
        top=t;


}



int pre(char x){
    if(x=='+' || x=='-')
    return 1;
    else if(x=='*' || x=='/')
    return 2;
    return 0;
}

int getNextToken(char x)
{
    if(x=='+' || x=='-' || x=='*' || x=='/')
    return 0;
    else
    return 1;
}



char pop()
{
    struct Node *t;
    char x=-1;
    if(top==NULL)
    printf("Stack is empty\n");
    else
    {
        t=top;
        top=top->next;x=t->data;
        free(t);

    }
    return x;
}

char * infixToPostfix(char *infix)
{
    int i=0,j=0;
    char *postfix;
    int len=strlen(infix);
    postfix=(char *)malloc((len+1)*sizeof(char));
    while(infix[i]!='\0')
    {
        if(getNextToken(infix[i]))
        postfix[j++]=infix[i++];
        else
        {
            if(pre(infix[i])>pre(top->data))
            push(infix[i++]);
            else
            {
                postfix[j++]=pop();
            }
        }
    }
                while(top!=NULL)
                postfix[j++]=pop();
                postfix[j]='\0';
                return postfix;
                return postfix;
}
char * infixToPrefix(char * infix)
{
    return infixToPostfix(strrev(infix));//strrev is to send reverse of a string


}

int main()
{
    char infix[50];
    printf("enter the expression");
    scanf("%s",infix);
    push('#'); //initial value in the stack
    char *prefix=infixToPrefix(infix);
    printf("%s ",strrev(prefix));
    return 0;
}
